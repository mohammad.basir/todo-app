# Directories
alias ll='ls -FGlAhp'
alias lz='ls --human-readable --size -1 -S --classify' #Sort by file size
alias left='ls -t -1' #Sort by modification time

alias ..="cd ../"
alias ...="cd ../../"
alias ....="cd ../../../"
alias .....="cd ../../../../"

alias df="df -h" #disk usage
alias fu="du -ch" # folder usage
alias tfu="du -sh" #total folder usage

# PHP
alias composer='composer'
alias composerd='composer dumpautoload'
alias composero='composer outdated --direct'
alias composerr='composer require'
alias composeru='composer update'

alias art='php artisan'
alias tinker="php artisan tinker"

alias y='yarn'
alias yr='yarn run'

alias test='vendor/bin/phpunit'

# Functions
function mcd () # mkdir "cd"
{
    mkdir -p -- "$1" &&
    cd -P -- "$1"
}

function cl() # cd ls
{
    DIR="$*";
        # if no DIR given, go home
        if [ $# -lt 1 ]; then
                DIR=$HOME;
    fi;
    builtin cd "${DIR}" && \
    # use your preferred ls command
        ls -F --color=auto
}

# from https://opensource.com/article/19/7/bash-aliases
