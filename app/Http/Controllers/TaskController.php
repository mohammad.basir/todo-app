<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        return Task::all();
    }

    public function show($id)
    {
        $task = Task::find($id);
        return $task ?: 'not found!';
    }

    public function store(Request $request)
    {
        if ($request->title) {
            $success = Task::create([
                'title' => $request->title,
            ]);
            if ($success) {
                return 'your task is added successfully';
            }
        }
        return 'error! please write a valid title';
    }

    public function update($id, Request $request)
    {
        $task = Task::find($id);
        if ($task) {
            $success = $task->update([
                'title' => $request->title,
            ]);
            if ($success) {
                return 'The task is updated successfully';
            }
        }
        return 'not found!';
    }

    public function destroy($id)
    {
        $task = Task::find($id);
        if ($task) {
            $task->delete();
            return 'deleted';
        }
        return 'not found!';
    }
}
