# todo-app

A simple Todo list based on RESTful API.

## Setup

```bash
git clone https://gitlab.com/mohammad.basir/todo-app.git
cd todo-app
cp .env.example .env

docker compose up --build

docker-compose exec php bash
bash docker/php/setup.bash

php artisan serve --host=0.0.0.0
```

## Testing

It is a Postman collection containing the project routes:
https://www.getpostman.com/collections/051c2b14c8d6a04e37f0
